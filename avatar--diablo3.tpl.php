<?php

/**
 * @file
 * A basic template for mmotoon entities
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The name of the mmotoon
 * - $url: The standard URL for viewing a mmotoon entity
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - mmotoon-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 * 
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if (!$page && $view_mode == 'full' || $view_mode != 'full'): ?>
    <h2<?php print $title_attributes; ?>>
        <a href="<?php print $url; ?>" rel="<?php print $title_link_rel; ?>" class="<?php print $title_link_class; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <div class="content"<?php print $content_attributes; ?>>
  <div class="diablo3-bg-image" style="background-image:url(<?php echo $bg_image; ?>);">
  <div class="sholder-slot">
  <a href="http://us.battle.net/d3/en/item/amice"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['shoulders']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="amulet-slot">
  <a href="http://us.battle.net/d3/en/item/amulet-1PQsvE"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['neck']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="glove-slot">
  <a href="http://us.battle.net/d3/en/item/gloves"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['hands']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  <div class="chest-slot">
  <a href="http://us.battle.net/d3/en/item/splint-cuirass"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['torso']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  <div class="wrist-slot">
  <a href="http://us.battle.net/d3/en/item/steady-strikers"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['bracers']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="rh-ring-slot">
  <a href="http://us.battle.net/d3/en/item/ring-1fCyAh"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['rightFinger']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
    <div class="legs-slot">
  <a href="http://us.battle.net/d3/en/item/chain-leggings"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['legs']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  <div class="lh-ring-slot">
    <a href="http://us.battle.net/d3/en/item/ring-1fCyAi"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['leftFinger']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="helm-slot">
  <a href="http://us.battle.net/d3/en/item/coif"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['head']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="belt-slot">
  <a href="http://us.battle.net/d3/en/item/hide-belt"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['waist']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  
  <div class="main-hand-slot"> <!--   item-bg-orange -->
    <a href="http://us.battle.net/d3/en/item/broad-axe"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['mainHand']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  <div class="feet-slot">
    <a href="http://us.battle.net/d3/en/item/boots"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['feet']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  </div>
  
  <div class="off-hand-slot">
  <?php if ($items['offHand']) {?>
    <a href="http://us.battle.net/d3/en/item/targe-shield"><img src="http://us.media.blizzard.com/d3/icons/items/large/<?php echo $items['offHand']['icon'] . '_' . $item_class_name . '_' . $gender; ?>.png" /></a>
  <?php } ?>
  </div>
  
  
  <div class="attributes">
  <h3><?php echo $title; ?></h3>
  <?php
  echo $stats['damageIncrease']['label'] . ': '. $stats['damageIncrease']['value'] . "<br />\n";
  echo $stats['damageReduction']['label'] . ': '. $stats['damageReduction']['value'] . "<br />\n";
  echo $stats['critChance']['label'] . ': '. $stats['critChance']['value'] . "<br />\n";
  echo $stats['life']['label'] . ': '. $stats['life']['value'] . "<br />\n";
  echo $stats['strength']['label'] . ': '. $stats['strength']['value'] . "<br />\n";
  echo $stats['intelligence']['label'] . ': '. $stats['intelligence']['value'] . "<br />\n";
  echo $stats['vitality']['label'] . ': '. $stats['vitality']['value'] . "<br />\n";
  echo $stats['armor']['label'] . ': '. $stats['armor']['value'] . "<br />\n";
  echo $stats['coldResist']['label'] . ': '. $stats['coldResist']['value'] . "<br />\n";
  echo $stats['fireResist']['label'] . ': '. $stats['fireResist']['value'] . "<br />\n";
  echo $stats['lightningResist']['label'] . ': '. $stats['lightningResist']['value'] . "<br />\n";
  echo $stats['poisonResist']['label'] . ': '. $stats['poisonResist']['value'] . "<br />\n";
  echo $stats['arcaneResist']['label'] . ': '. $stats['arcaneResist']['value'] . "<br />\n";
  echo $stats['damage']['label'] . ': '. $stats['damage']['value'] . "<br />\n";
  ?>
  </div>
  </div>
  <?php
      print render($content);
    ?>
  </div>
</div>
