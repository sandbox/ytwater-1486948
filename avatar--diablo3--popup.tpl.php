<?php

/**
 * @file
 * A basic template for avatar entities
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The name of the mmotoon
 * - $url: The standard URL for viewing a mmotoon entity
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - mmotoon-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="summary-popup <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<img src="<?php echo $portrait_url; ?>" width="48" height="48" class="diablo3-popup-img" />

	<div class="popup-frame">
  	<div class="popup-text-wrapper">
  	  <div class="<?php print $title_link_class; ?>"><?php print $title; ?></div>
    <?php
      print render($content);
    ?>
  	  
  	</div>
  </div>
</div>
