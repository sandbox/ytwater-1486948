<?php

/**
 * Implement hook_views_default_views().
 */
function diablo3_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'diablo_roster';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'avatar';
  $view->human_name = 'Diablo Roster';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Roster';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Avatar: Avatar ID */
  $handler->display->display_options['fields']['aid']['id'] = 'aid';
  $handler->display->display_options['fields']['aid']['table'] = 'avatar';
  $handler->display->display_options['fields']['aid']['field'] = 'aid';
  $handler->display->display_options['fields']['aid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['aid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['aid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['aid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['aid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['aid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['aid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['aid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['aid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['aid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['aid']['format_plural'] = 0;
  /* Field: Avatar: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'avatar';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['name']['alter']['path'] = 'avatar/[aid]';
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  /* Field: Avatar: Level */
  $handler->display->display_options['fields']['field_level']['id'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['table'] = 'field_data_field_level';
  $handler->display->display_options['fields']['field_level']['field'] = 'field_level';
  $handler->display->display_options['fields']['field_level']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_level']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_level']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_level']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_level']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_level']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_level']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_level']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_level']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_level']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_level']['field_api_classes'] = 0;
  /* Field: Avatar: Gender */
  $handler->display->display_options['fields']['field_gender']['id'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['table'] = 'field_data_field_gender';
  $handler->display->display_options['fields']['field_gender']['field'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_gender']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_gender']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_gender']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_gender']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_gender']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_gender']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_gender']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_gender']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_gender']['field_api_classes'] = 0;
  /* Field: Avatar: Class */
  $handler->display->display_options['fields']['field_class']['id'] = 'field_class';
  $handler->display->display_options['fields']['field_class']['table'] = 'field_data_field_class';
  $handler->display->display_options['fields']['field_class']['field'] = 'field_class';
  $handler->display->display_options['fields']['field_class']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_class']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_class']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_class']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_class']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_class']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_class']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_class']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_class']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_class']['field_api_classes'] = 0;
  /* Filter criterion: Avatar: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'avatar';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'diablo3' => 'diablo3',
  );
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'roster';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Roster';
  $handler->display->display_options['menu']['description'] = 'Listing of all Diablo 3 Characters';
  $handler->display->display_options['menu']['weight'] = '3';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  
  
  $views[$view->name] = $view;
  return $views;
}